
# Spawn manager

A simple spawn system to handle several hundred units.



## Tools

- Unity 2021.3.11f1 LTS
- Universal Render Pipeline
## Features

- A simple input manager to spawn a certain number of units and remove them all from the scene,
- Spawned units receive a random destination position and random arrival time, and receive another random destination upon completion, 
- Spawned units move with DOTween,



## To do list

 - Pathfinding to avoid obstacles,
 - Animations for units,
 - Selecting units with the mouse and issuing simple orders,
 

## Author

- [@T.Janicki](https://www.linkedin.com/in/tomasz-janicki-809a641b9/)
