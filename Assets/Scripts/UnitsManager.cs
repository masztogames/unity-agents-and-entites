using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

namespace TJ
{
    public class UnitsManager : MonoBehaviour
    {
        public static UnitsManager Instance { get; private set; }

        [Tooltip("A prefab object to spawn")]
        [SerializeField] private GameObject _unitPrefab;
        [Tooltip("A text field to display the current number of units being spawned")]
        [SerializeField] private TextMeshProUGUI _spawnedUnitText;
        [Tooltip("The minimum time it takes for a spawned object to reach its destination")]
        [Range(3f, 10f)]
        [SerializeField] private float _minTimeToReachTarget = 5f;
        [Tooltip("The maximum time it takes for a spawned object to reach its destination")]
        [Range(5f, 15f)]
        [SerializeField] private float _maxTimeToReachTarget = 10f;

        private List<GameObject> _units = new List<GameObject>();
        private int _currentSpawnedUnits;

        private Vector3 RandomPosition() => new Vector3(Random.Range(-30f, 30f), 0, Random.Range(-30f, 30f));
        private float RandomTime() => Random.Range(_minTimeToReachTarget, _maxTimeToReachTarget);

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            _maxTimeToReachTarget = _maxTimeToReachTarget <= _minTimeToReachTarget ? _minTimeToReachTarget + 1 : _maxTimeToReachTarget;
        }

        public void SpawnUnits(int value)
        {
            for (int i = 0; i < value; i++)
            {
                Debug.Log("Spawn unit");
                GameObject unit = Instantiate(_unitPrefab);
                _units.Add(unit);
                unit.transform.parent = gameObject.transform;
                SetNewTarget(unit);
            }
            _currentSpawnedUnits += value;
            _spawnedUnitText.text = "Spawned units " + _currentSpawnedUnits.ToString();
        }

        private void SetNewTarget(GameObject unit)
        {
            Vector3 target = RandomPosition();
            unit.transform.LookAt(target);
            unit.transform.DOMove(target, RandomTime()).OnComplete(() => SetNewTarget(unit));
        }

        public void DeleteAllUnits()
        {
            foreach (GameObject unit in _units)
            {
                Destroy(unit);
            }
            _currentSpawnedUnits = 0;
            _spawnedUnitText.text = "Spawned units " + _currentSpawnedUnits.ToString();
        }
    }
}
