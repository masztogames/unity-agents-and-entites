using UnityEngine;

namespace TJ
{
    public class InputManager : MonoBehaviour
    {
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                UnitsManager.Instance.SpawnUnits(1);
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                UnitsManager.Instance.SpawnUnits(10);
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                UnitsManager.Instance.SpawnUnits(100);
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                UnitsManager.Instance.DeleteAllUnits();
            }
        }
    }
}
